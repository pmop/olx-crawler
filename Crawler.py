import logging
import shelve
import time
from copy import deepcopy
from requests_html import HTMLSession
from olx import build_olxadvert, _olx_item_sel_item


class Crawler:
    def __init__(self, time_r=5, time_i=300, session_handler=HTMLSession):
        self.db = shelve.open('olx.db', flag='c')
        self.prefix = 'https://al.olx.com.br/'
        self.reqs = dict({'hoje': '?q=hoje', 'aluguel': 'imoveis/aluguel?pe=400'})
        self.db_asdict = dict()
        self.exclude_list = []
        self.refresh_index = True
        self.sleep_handler = None
        self.sleep_between_refresh = time_i
        self.sleep_between_requests = time_r
        self.new_items_added_callback = None
        self.is_running = True
        self.session = HTMLSession()
        self.user_agent = 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
        self.init_db()

    def db_sync(self):
        if len(self.db_asdict.items()) > 0:
            for key, value in self.db_asdict.items():
                self.db[key] = value
            self.db.sync()

    def init_db(self):
        self.db_asdict = dict(self.db)
        if len(self.db_asdict.items()) is 0:
            for query, _ in self.reqs.items():
                self.db_asdict[query] = dict()

    def build_request_urls(self):
        for key in self.reqs.keys():
            self.reqs[key] = self.prefix + self.reqs[key]

    def run(self):
        self.build_request_urls()
        dict_as_list = [{'query': x, 'url': y} for x, y in self.reqs.items()]

        while self.is_running:
            if self.refresh_index:
                # Wont do it multithreaded to prevent server from blocking quickly repeated requests
                # results = pool.map(func=self.scrap_page_and_process,iterable=dict_as_list)
                results = []
                for item in dict_as_list:
                    results.append(self.scrap_page_and_process(item))
                    if self.sleep_between_requests > 0:
                        logging.debug(f"Crawler: going to sleep before next request: {self.sleep_between_requests}")
                        time.sleep(self.sleep_between_requests)
                for result in results:
                    r_query = result['query']
                    r_olx_items = result['result']
                    self.process_olx_items(r_query, r_olx_items)
                self.refresh_index_end_task()
                self.sleep_and_dobackground(self.sleep_between_refresh)

    def refresh_index_end_task(self):
        ...

    # Scraps information from item['url']
    # Returns result with result['query'] the query made and result['result'] the built items.
    # Returns empty dict in case of something goes wrong.
    def scrap_page_and_process(self, item):
        query = item['query']
        url = item['url']
        result = []
        for key, value in self.reqs.items():
            try:
                r = self.session.get(url, headers={'user-agent': self.user_agent})
                olx_advert_items = r.html.find(_olx_item_sel_item)
                result = olx_advert_items
            except ConnectionError as con_error:
                logging.warning(f"Crawler: failed to send request. url: {value}. Cause: {con_error}.")
            finally:
                if len(result) > 0:
                    result = list(map(build_olxadvert, result))
                    result = {'query': query, 'result': result}
                return result

    def new_items_added(self, new_items_list):
        for item_dict in new_items_list:
            query = item_dict['query']
            item = item_dict['item']
            self.db_asdict[query][item['id']] = item

    def process_olx_items(self, key, olx_items):
        new_items = []
        for item in olx_items:
            id_ = item['id']
            if id_ not in self.db_asdict[key].keys():
                new_items.append({'query': key, 'item': item})
                logging.debug(f"Crawler: new item found. id: {id_}, query: {key}")
        if len(new_items) > 0:
            self.new_items_added(new_items)

    # Sleeps for given time or until thread shouldnt be running anymore
    def sleep_and_dobackground(self, sleep_time):
        if self.is_running:
            logging.debug(f"Crawler: sleeping until next refresh: {sleep_time} seconds.")
            begin = time.time()
            sleep_time = float(sleep_time)
            try:
                logging.debug("Crawler: db sync.")
                self.db_sync()
            except ValueError:
                logging.warning("Crawler: tried to write to a closed shelf.")

            while time.time() - begin < sleep_time and self.is_running:
                delta = time.time() - begin
                if delta % 10 == 0:
                    logging.debug(f"Crawler: slept {delta} seconds.")

    def stop(self):
        self.is_running = False
        try:
            self.db.close()
            self.session.close()
        except ValueError:
            logging.warning("Crawler: Failed to close database or session.")
        finally:
            logging.debug("Crawler: EXITING!.")

    def get_db_asdict_copy(self):
        return deepcopy(self.db_asdict)
