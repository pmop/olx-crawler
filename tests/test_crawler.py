from unittest import TestCase
from tests.mocks import NoLoopCrawler
from ThreadedCrawler import ThreadedCrawler
from time import sleep


class TestCrawler(TestCase):
    def test_get(self):
        test_crawler = NoLoopCrawler()
        test_crawler.run()
        assert len(test_crawler.get_db_asdict_copy().items()) > 0

    def test_threaded_crawler(self):
        threaded_crawler = ThreadedCrawler()
        threaded_crawler.start()
        sleep(45)
        threaded_crawler.stop()
        assert len(threaded_crawler.get_db_asdict_copy().items()) > 0

