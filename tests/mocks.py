from Crawler import Crawler
from requests_html import HTMLSession, HTML
from requests import Response


class MockResponse(Response):
    def __init__(self):
        super(Response, self).__init__()


class MockHTMLSession(HTMLSession):
    def __init__(self):
        super(MockHTMLSession, self).__init__()

    def get(self, url, **kwargs):
        file = open('olx.html', mode='r')
        html = HTML(file)
        file.close()
        return html


class NoLoopCrawler(Crawler):
    def __init__(self):
        super(NoLoopCrawler, self).__init__(time_r=0, time_i=10, session_handler=HTMLSession)

    def refresh_index_end_task(self):
        self.db_sync()
        self.stop()
