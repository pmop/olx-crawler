import logging
import re
from requests_html import HtmlElement

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

_olx_item_sel_id = 'name'
_olx_item_sel_link = 'href'
_olx_item_sel_title = 'title'
_olx_item_sel_price = '.OLXad-list-price'
_olx_item_sel_where = '.OLXad-list-line-2>.detail-region'
_olx_item_sel_when = '.col-4'
_olx_item_sel_default_img = 'https://static.bn-static.com/img-49158/desktop/transparent.png'
_olx_item_sel_img = 'img.image'
_olx_item_sel_item = '.list>.item>.OLXad-list-link'


def build_olxadvert(data):
    attrs = data.attrs
    id_ = attrs[_olx_item_sel_id]
    link_ = attrs[_olx_item_sel_link]
    title_ = attrs[_olx_item_sel_title]
    where_ = data.find(_olx_item_sel_where, first=True)
    if where_ is not None:
        where_ = where_.text
    else:
        where_ = ''

    when_ = [x.text for x in data.find(_olx_item_sel_when)]

    price_ = data.find(_olx_item_sel_price, first=True)
    if price_ is not None:
        price_ = price_.text  # use regex here
        price_ = re.search(r'\d+', price_.strip('.')).group(0)
        price_ = int(price_)
    else:
        price_ = ""

    img_link_ = data.find(_olx_item_sel_img, first=True)
    if img_link_ is not None:
        img_link_ = img_link_.attrs['src']
        if img_link_ is _olx_item_sel_default_img:
            img_link_ = ''
    else:
        img_link_ = ""

    return {'id': id_, 'title': title_, 'link': link_, 'when': when_,
            'where': where_, 'price': price_, 'img_link': img_link_}

# def list(bot, update, args):
#     msg = ''
#     qdict = global_base[args]
#     for key, value in qdict.items():
#         title = value['title']
#         link = value['link']
#         when = value['when']
#         where = value['where']
#         price = value['price']
#         img_link = value['img_link']
#         if img_link is not '':
#             msg = f'[{title}]({link})  \n'
#         else:
#             msg = f'{title}  \n'
#         msg += f'{price}  \n{where}  \n{when}'
#         print(msg)
#         msg = ''
#         bot.send_message(chat_id=update.message.chat_id, text=msg, parse_mode=telegram.ParseMode.HTML)

# def main():
#     token = bot_token
#     updater = Updater(token=token)
#     dispatcher = updater.dispatcher
#     start_handler = CommandHandler('start', start)
#     list_handler = CommandHandler('list', list, pass_args=True)
#     dispatcher.add_handler(start_handler)
#     dispatcher.add_handler(list_handler)
#     updater.start_polling()
#
#     enable_requesting = True
#     update_index_timer = 300
#
#     db = shelve.open('olx.db', flag='c')
#     update_index_thread = RepeatedTimer(update_index_timer, update_index, db,
#                                         enable_requesting, reqs)
#     update_index_thread.start()
#     run_main_thread = True
#
#     try:
#         while run_main_thread:
#             pass
#     finally:
#         run_main_thread = False
#         updater.stop()
#         update_index_thread.stop()
#
#     db.sync()
#     db.close()
#     logging.log("Exiting main thread")
#
#
# main()
